from Commands import *
from Handlers import *
import json
from model import *

from Session.MsgBoxController import MsgBoxController
from Session.ImageController import ImageController

from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, SourceGroup, SourceUser, SourceRoom
)

import sys, inspect

modules = inspect.getmembers(sys.modules[__name__], inspect.ismodule)

commands = []
handlers = []
controllers = [MsgBoxController, ImageController]

for module in modules:
    if not module[0].endswith('Handler') and not module[0].endswith('Command'):
        continue

    classMembers = inspect.getmembers(module[1], inspect.isclass)

    for clsMember in classMembers:
        if clsMember[0].endswith('Handler'):
            handlers.append(clsMember[1])
        elif clsMember[0].endswith('Command'):
            commands.append(clsMember[1])

def findCommandHandler(cmd):
    global commands, handlers

    response = []
    userCommand = cmd.lower() + 'command'
    userHandler = cmd.lower() + 'handler'

    for command in commands:
        if command.__name__.lower() == userCommand:
            response.append(command)

    for handler in handlers:
        if handler.__name__.lower() == userHandler:
            response.append(handler)

    if len(response) == 2:
        return response

    else:
        raise Exception('Command %s not found' % userCommand)

def findSession(event):
    try:
        tipe = event.source
        if isinstance(tipe, SourceGroup):
            user = User.get(User.sourceId == event.source.group_id)
        elif isinstance(tipe, SourceRoom):
            user = User.get(User.sourceId == event.source.room_id)
        else:
            user = User.get(User.sourceId == event.source.user_id)

        if user.session == None:
            return None

        return json.loads(user.session)
    except Exception as e:
        user = createSession(event)
        return None

def createSession(event):
    tipe = event.source
    if isinstance(tipe, SourceGroup):
        sourceId = event.source.group_id
        tipe = 'group'
    elif isinstance(tipe, SourceRoom):
        sourceId = event.source.room_id
        tipe = 'room'
    else:
        sourceId = event.source.user_id
        tipe = 'user'

    user = User(sourceId=sourceId, tipe=tipe)
    user.save()
    personalUser = User(sourceId=event.source.user_id, tipe='user')
    personalUser.save()
    return user

def getUser(event):
    tipe = event.source
    if isinstance(tipe, SourceGroup):
        sourceId = event.source.group_id
        tipe = 'group'
    elif isinstance(tipe, SourceRoom):
        sourceId = event.source.room_id
        tipe = 'room'
    else:
        sourceId = event.source.user_id
        tipe = 'user'

    user = User.get(User.sourceId == sourceId)
    return user

def resolve(client, event, tipe='text'):
    session = findSession(event)

    if tipe == 'text':
        if event.message.text == '!exit':
            user = getUser(event)
            user.session = None
            user.save()
            raise Exception("You have been exited")

    if session != None:
        sessionResolve(client, event, session)

    elif tipe == 'text':
        if event.message.text[0] == '!':
            cmd = event.message.text.split()[0][1:]
        elif event.message.text[0] == '#':
            cmd = 'getimage'

    elif tipe == 'image':
        cmd = 'image'

    try:
        print(cmd)
    except:
        return None

    command, handler = findCommandHandler(cmd)
    command = command.parse(event)
    handler.execute(client, command)

def sessionResolve(client, event, session, tipe='text'):
    global controllers
    sessName = session['name'] + 'controller'
    state = session['state']
    substate = session['substate']
    for controller in controllers:
        if controller.__name__.lower() == sessName:
            ctrl = controller(client, event, state, substate)
            ctrl.parseState()

    raise Exception('Command %s not found' % session['name'])