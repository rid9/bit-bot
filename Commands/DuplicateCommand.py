from Commands.BaseCommand import BaseCommand

from linebot.models import SourceUser


class DuplicateCommand(BaseCommand):
    @staticmethod
    def name():
        return 'Duplicate Command'

    @staticmethod
    def help():
        return 'This command will link your images with the other account'

    def isAllowed(self):
        user = isinstance(self.event.source, SourceUser)

        return user