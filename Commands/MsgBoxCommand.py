from Commands.BaseCommand import BaseCommand

from linebot.models import SourceUser

class MsgBoxCommand(BaseCommand):
    @staticmethod
    def name():
        return 'MsgBox Command'

    @staticmethod
    def help():
        return 'This command will show you the msgbox menu. Usage !msgbox'

    def isAllowed(self):
        user = isinstance(self.event.source, SourceUser)

        return user