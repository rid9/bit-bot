from Commands.BaseCommand import BaseCommand

#Echo Command - For testing purposes

class EchoCommand(BaseCommand):
	@staticmethod
	def name():
		return 'Echo Command'

	@staticmethod
	def help():
		return 'This command will echo your message. Usage : !echo <message>'