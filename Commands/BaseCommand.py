from linebot.models import (
    SourceGroup, SourceUser, SourceRoom
)

from model import *

# Base Command - Maybe useful in the future

class BaseCommand():
    def __init__(self, event, substate):
        self.event = event
        self.substate = substate

    @classmethod
    def parse(cls, event, substate=''):

        return cls(event, substate)

    def getMessagePerWord(self):
        return self.event.message.text.split()

    def getMessage(self):
        return ' '.join(self.getMessagePerWord()[1:])

    def getReplyToken(self):
        return self.event.reply_token

    def getFullMessage(self):
        return self.event.message.text

    def isAllowed(self):
        user = isinstance(self.event.source, SourceUser)
        group = isinstance(self.event.source, SourceGroup)
        room  = isinstance(self.event.source, SourceRoom)

        return user or group or room

    def sourceType(self):
        source = self.event.source

        if isinstance(source, SourceUser):
            self.source = source.user_id
            return 'user'

        elif isinstance(source, SourceGroup):
            self.source = source.group_id
            return 'group'

        else:
            self.source = source.room_id
            return 'room'

    def getUser(self):
        self.sourceType()
        sourceId = self.source

        user = User.get(User.sourceId == sourceId)
        return user

    def getPersonalUser(self):
        source = self.event.source.user_id

        user = User.get(User.sourceId == source)
        return user