from Commands.BaseCommand import BaseCommand

#Image Command -> to save user images

class GetImageCommand(BaseCommand):
    @staticmethod
    def name():
        return 'Get Image Command'

    @staticmethod
    def help():
        return 'You can get your saved picture by sending #your-picture-tag'

    def getMessageId(self):
        return self.event.message.id