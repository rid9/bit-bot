from Commands.BaseCommand import BaseCommand
from linebot.models import SourceUser

#Image Command -> to save user images

class ImageCommand(BaseCommand):
    @staticmethod
    def name():
        return 'Image Command'

    @staticmethod
    def help():
        return 'By sending a picture, your picture will be uploaded and saved to your personal gallery'

    def getMessageId(self):
        return self.event.message.id

    def isAllowed(self):
        user = isinstance(self.event.source, SourceUser)

        return user