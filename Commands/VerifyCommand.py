from Commands.BaseCommand import BaseCommand


class VerifyCommand(BaseCommand):
    @staticmethod
    def name():
        return 'Verify Command'

    @staticmethod
    def help():
        return 'This command will verify your secondary account. Usage : !verify <unique code>'