from peewee import *
import datetime

db = MySQLDatabase('bitbot', user='root', password='abcdefg')

class BaseModel(Model):
	 class Meta:
	 	database = db

class User(BaseModel):
	sourceId = CharField()
	tipe = CharField()
	session = CharField(null=True)
	permission = CharField(null=True)
	rank = IntegerField(default=1)
	createdAt = DateTimeField(default=datetime.datetime.now())

class MsgBox(BaseModel):
	ownerId = ForeignKeyField(User, related_name='msgbox')
	uniqueId = CharField()
	size = IntegerField(default=5)
	limit = IntegerField(default=1)
	tipe = CharField(default='Anonymous', null=True)
	status = BooleanField(default=True)
	createdAt = DateTimeField(default=datetime.datetime.now())

class MsgBoxMessage(BaseModel):
	msgBox = ForeignKeyField(MsgBox, related_name='messages')
	user = ForeignKeyField(User, related_name='messages')
	message = CharField()
	createdAt = DateTimeField(default=datetime.datetime.now())

class UserImage(BaseModel):
	user = ForeignKeyField(User, related_name='images')
	link = CharField()
	tag = CharField()
	createdAt = DateTimeField(default=datetime.datetime.now())

class Verification(BaseModel):
	uniqueId = CharField()
	user = ForeignKeyField(User, related_name='verificationkeys')
	createdAt = DateTimeField(default=datetime.datetime.now())

class Relation(BaseModel):
	parentUser = ForeignKeyField(User, related_name='childs')
	childUser = ForeignKeyField(User, related_name='parents')
	createdAt = DateTimeField(default=datetime.datetime.now())