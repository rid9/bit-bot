from Handlers.BaseHandler import BaseHandler
from model import *

class VerifyHandler(BaseHandler):
    def handle(self):
        try:
            user = self.command.getPersonalUser()
            verification = Verification.get(Verification.uniqueId == self.command.getMessage())
            parent = verification.user[0]
            relation = Relation(parentUser=parent, childUser=user)
            relation.save()
        except:
            raise Exception("Invalid key")

        self.setMessage([{
            "message" : "Your account is now verified",
            "type" : "text"
        }])

        self.send()