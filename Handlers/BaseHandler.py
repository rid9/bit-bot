from linebot.models import SourceGroup, SourceRoom, TextSendMessage, ImageSendMessage
import json
from model import *
#Base Handler

class BaseHandler():
    def __init__(self, client, command):
        self.client = client
        self.command = command

    @classmethod
    def execute(cls, client, command):
        if not(command.isAllowed()):
            raise Exception("This command can't be executed from %s" % command.sourceType())
        handler = cls(client, command)
        handler.handle()

    def handle(self):
        raise Exception('Command %s not found' % command.name)

    def setMessage(self, data):
        toSend = []
        for message in data:
            if message['type'] == 'text':
                toSend.append(TextSendMessage(text=message['message']))
            elif message['type'] == 'image':
                toSend.append(ImageSendMessage(original_content_url=message['link'],
                    preview_image_url=message['link']))
        self.toSend = toSend

    def send(self):
        if self.toSend == []:
            return None
        self.client.reply_message(self.command.getReplyToken(), self.toSend)
        self.toSend = []

    def addSubstate(self):
        self.command.sourceType()
        user = User.get(User.sourceId == self.command.source)

        session = json.loads(user.session)

        session['substate'] += 1

        user.session = json.dumps(session)
        user.save()

    def addSessionData(self,user, data):
        session = json.loads(user.session)
        session['data'] = data
        session['substate'] += 1
        user.session = json.dumps(session)

        user.save()

    def getSessionData(self,user):
        return json.loads(user.session)['data']

    def deleteSession(self):
        self.command.sourceType()
        user = User.get(User.sourceId == self.command.source)

        user.session = None
        user.save()