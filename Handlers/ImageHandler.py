from Handlers.BaseHandler import BaseHandler
from random import randint

def randomChar(c):
    a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    b = ''
    for x in range(c):
        b += a[randint(0, len(a)-1)]

    return b

class ImageHandler(BaseHandler):
    def handle(self):
        randName = randomChar(8)
        user = self.command.getUser()
        user.session = '''{"name" : "image", "state" : "input", "substate" : 1, "data" : "%s"}''' % randName
        user.save()

        mid = self.command.getMessageId()

        image = self.client.get_message_content(mid)

        with open(randName, 'wb') as f:
            for chunk in image.iter_content():
                f.write(chunk)
            f.close()

        message = {
            "type" : "text",
            "message" : "What is the tag for this picture?"
        }

        self.setMessage([message])
        self.send()