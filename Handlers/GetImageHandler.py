from Handlers.BaseHandler import BaseHandler

class GetImageHandler(BaseHandler):
    def handle(self):
        try:
            main = self.command.getPersonalUser()
        except:
            raise Exception("We can't find you in our DB")

        tag = self.command.getFullMessage()[1:]
        secondary = main.parents

        for image in main.images:
            if image.tag == tag:
                self.setMessage([{
                    "type" : "image",
                    "link" : image.link
                }])
                self.send()
                return None

        for child in secondary:
            user = child.parentUser
            for image in user.images:
                if image.tag == tag:
                    self.setMessage([{
                        "type" : "image",
                        "link" : image.link
                    }])
                    self.send()
                    return None

        raise Exception("Image #%s not found" % tag)