#Echo Handler - For Testing Purposes
from Handlers.BaseHandler import BaseHandler

class EchoHandler(BaseHandler):
	def handle(self):
		message = self.command.getMessage()
		echo = [{'type' : 'text', 'message' : message}]
		self.setMessage(echo)
		self.send()