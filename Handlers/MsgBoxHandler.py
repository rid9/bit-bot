#Echo Handler - For Testing Purposes
from Handlers.BaseHandler import BaseHandler
from linebot.models import TemplateSendMessage, MessageTemplateAction, ButtonsTemplate

class MsgBoxHandler(BaseHandler):
    def handle(self):
        user = self.command.getUser()
        user.session = '''{"name" : "msgbox", "state" : "", "substate" : 1}'''
        user.save()

        message = TemplateSendMessage(
            alt_text='Message Box',
            template=ButtonsTemplate(
                title='Message Box Menu',
                text='Please select',
                actions=[
                    MessageTemplateAction(
                        label='Create',
                        text='create'
                    ),
                    MessageTemplateAction(
                        label='See MsgBox',
                        text='see'
                    ),
                    MessageTemplateAction(
                        label='Delete',
                        text='delete'
                    ),
                    MessageTemplateAction(
                        label='Input',
                        text='input'
                    )
                ]
            )
        )

        self.toSend = message
        self.send()