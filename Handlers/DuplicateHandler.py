from Handlers.BaseHandler import BaseHandler
from model import *

from random import randint

def randomChar(c):
    a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    b = ''
    for x in range(c):
        b += a[randint(0, len(a)-1)]

    return b

class DuplicateHandler(BaseHandler):
    def handle(self):
        user = self.command.getUser()
        randChar = randomChar(8)
        verification = Verification(uniqueId=randChar, user=user)
        verification.save()

        self.setMessage([{
            "message" : "Please copy paste this message below on your other account to this bot",
            "type" : "text"
            },
            {
            "message" : "!verify %s" % randChar,
            "type" : "text"
            }
        ])

        self.send()