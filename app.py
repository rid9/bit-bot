from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, ImageMessage
)

from config import *
from model import *

from CommandBus import resolve

app = Flask(__name__)

client = LineBotApi(ACCESS_TOKEN)
handler = WebhookHandler(SECRET_TOKEN)

@app.before_request
def _db_connect():
    if db.is_closed():
        db.connect()

@app.teardown_request
def _db_close(exc):
    if not db.is_closed():
        db.close()

@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'

@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    try:
        resolve(client, event, 'text')
    except Exception as e:
        client.reply_message(event.reply_token, TextSendMessage(text=str(e)))

@handler.add(MessageEvent, message=ImageMessage)
def handle_image(event):
    try:
        resolve(client, event, 'image')
    except Exception as e:
        client.reply_message(event.reply_token, TextSendMessage(text=str(e)))

if __name__ == "__main__":
    app.run()