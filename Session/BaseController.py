#BaseController - Might be needed later

from linebot.models import (
    SourceGroup, SourceUser, SourceRoom
)

import sys, inspect

from Session.Commands import *
from Session.Handlers import *
from model import *
import json

class BaseController():
    def __init__(self, client, event, state, substate):
        self.client = client
        self.event = event
        self.state = state
        self.substate = substate

    def parseState(self):
        if not self.isAllowed():
            raise Exception('This command is not allowed for %s' % self.event.source.type)

        if self.state == "":
            user = self.getUser()
            session = json.loads(user.session)
            session['state'] = self.event.message.text
            session['substate'] = 1
            user.session = json.dumps(session)
            user.save()
            self.state = session['state']
            self.substate = session['substate']

        return self.execute()

    def getUser(self):
        if isinstance(self.event.source, SourceUser):
            source = self.event.source.user_id
        elif isinstance(self.event.source, SourceRoom):
            source = self.event.source.room_id
        else:
            source = self.event.source.group_id

        user = User.get(User.sourceId == source)
        return user

    def isAllowed(self):
        user = isinstance(self.event.source, SourceUser)

        return user

    def execute(self):
        raise Exception('This command does not exist')

    def localResolve(self, cmd, controller):
        modules = inspect.getmembers(sys.modules[__name__], inspect.ismodule)

        commands = []
        handlers = []

        for module in modules:
            if not module[0].endswith('Handler') and not module[0].endswith('Command'):
                continue

            classMembers = inspect.getmembers(module[1], inspect.isclass)

            for clsMember in classMembers:
                if clsMember[0].endswith('Handler'):
                    handlers.append(clsMember[1])
                elif clsMember[0].endswith('Command'):
                    commands.append(clsMember[1])

        response = []
        userCommand = cmd.lower() + controller +  'command'
        userHandler = cmd.lower() + controller + 'handler'

        for command in commands:
            if command.__name__.lower() == userCommand:
                response.append(command)

        for handler in handlers:
            if handler.__name__.lower() == userHandler:
                response.append(handler)

        if len(response) == 2:
            cmd = response[0].parse(self.event, self.substate)
            return response[1].execute(self.client, cmd)

        else:
            raise Exception('Command %s not found' % cmd)