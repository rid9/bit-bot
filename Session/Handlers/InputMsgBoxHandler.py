from Handlers.BaseHandler import BaseHandler
from model import *

class InputMsgBoxHandler(BaseHandler):
    def handle(self):
        user = self.command.getUser()
        message = self.command.getFullMessage()
        substate = self.command.substate

        if substate == 1:
            self.addSubstate()
            self.setMessage([{
                "message" : "Message Box Unique Id :",
                "type" : "text"
            }])
            self.send()

        elif substate == 2:
            uid = message
            msgbox = MsgBox.get(MsgBox.uniqueId == uid)

            if msgbox == None:
                raise Exception('No message box with such id found. Please try again.')

            self.addSessionData(user, uid)
            self.setMessage([{
                "message" : "Please enter your message",
                "type" : "text"
            }])

            self.send()

        elif substate == 3:
            uid = self.getSessionData(user)
            msgbox = MsgBox.get(MsgBox.uniqueId == uid)
            ownMessage = 0
            try:
                curInbox = len(msgbox.messages)
                for msg in msgbox.messages:
                    if msg.ownerId == user:
                        ownMessage += 1
            except:
                ownMessage = 0
                curInbox = 0
            if msgbox.size+1 < curInbox:
                raise Exception("The message box specified has reached it's maximum limit")

            if msgbox.limit <= ownMessage:
                raise Exception("You can only submit %d time(s)" % msgbox.limit)

            msg = MsgBoxMessage(msgBox=msgbox, user=user, message=self.command.getFullMessage())
            msg.save()

            self.deleteSession()
            self.setMessage([{
                "message" : "Your Message has been submitted!",
                "type" : "text"
            }])

            self.send()



