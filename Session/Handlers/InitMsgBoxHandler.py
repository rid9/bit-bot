from Handlers.BaseHandler import BaseHandler
from model import *
import json

class InitMsgBoxHandler(BaseHandler):
    def handle(self):
        user = self.command.getUser()
        message = self.command.getFullMessage().lower()

        session = json.loads(user.session)
        session['state'] = message

        user.session = json.dumps(session)
        user.save()

        self.setMessage([{
            "message" : "Send any key to continue",
            "type" : "text"
            }])

        self.send()