from Handlers.BaseHandler import BaseHandler
from model import *

class SeeMsgBoxHandler(BaseHandler):
    def handle(self):
        self.command.sourceType()
        sourceId = self.command.source

        user = User.get(User.sourceId == sourceId)
        msgbox = user.msgbox[0]

        messages = msgbox.messages
        msg = ''
        for message in messages:
            source = message.user.sourceId
            asker = self.client.get_profile(source).display_name
            if msgbox.tipe == 'Anonymous':
                asker = 'Anonymous'

            msg += '''%s\n-------\n%s\n-------\n''' % (asker, message.message)

        toSend = [{
            "message" : msg,
            "type" : "text"
        }]
        self.deleteSession()
        self.setMessage(toSend)
        self.send()