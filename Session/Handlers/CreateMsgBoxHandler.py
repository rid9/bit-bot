from Handlers.BaseHandler import BaseHandler
from model import *
import json

from random import randint

def randomChar(c):
    a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    b = ''
    for x in range(c):
        b += a[randint(0, len(a)-1)]

    return b

class CreateMsgBoxHandler(BaseHandler):
    def handle(self):
        user = self.command.getUser()
        message = self.command.getFullMessage()
        substate = self.command.substate

        if substate == 1:
            try:
                msgbox = user.msgbox[0]
                self.deleteSession()
                self.setMessage([{"message" : "You already have a messagebox, please delete it",
                    "type" : "text"
                    }])
            except:
                self.addSubstate()
                self.setMessage([{
                    "message" : "Message Box Size : ",
                    "type" : "text"
                }])
            self.send()

        elif substate == 2:
            size = message
            try:
                size = int(size)
            except:
                raise Exception("You must input only the number")

            msgbox = MsgBox(ownerId = user, size=size, uniqueId=randomChar(8))
            msgbox.save()
            self.addSubstate()

            self.setMessage([{
                "message" : "Anonymous input (y/n)",
                "type" : "text"
            }])

            self.send()

        elif substate == 3:
            anon = message.lower()
            user = self.command.getUser()
            msgbox = user.msgbox[0]

            if anon != 'y':

                msgbox.tipe = None
                msgbox.save()

            self.deleteSession()

            self.setMessage([
                {
                "message" : "Thank you! Here is your message box id",
                "type" : "text"
                },
                {
                "message" : msgbox.uniqueId,
                "type" : "text"
                }
            ])

            self.send()