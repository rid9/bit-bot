from Handlers.BaseHandler import BaseHandler
from model import *

class DeleteMsgBoxHandler(BaseHandler):
    def handle(self):
        user = self.command.getUser()
        msgbox = user.msgbox[0]

        for msg in msgbox.messages:
            msg.delete_instance()

        msgbox.delete_instance()
        user.session = None
        user.save()

        self.setMessage([{
            "message" : "Message box has been deleted!",
            "type" : "text"
        }])

        self.send()