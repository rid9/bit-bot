from Handlers.BaseHandler import BaseHandler
from model import *
import requests
import imghdr
import os

class InputImageHandler(BaseHandler):
    def handle(self):
        user = self.command.getUser()
        message = self.command.getFullMessage()
        substate = self.command.substate
        imageName = self.getSessionData(user)

        if substate == 1:
            file = open(self.getSessionData(user), 'rb')
            files = {
                "reqtype" : (None, "fileupload"),
                "fileToUpload" : (imageName, file, 'image/' +imghdr.what(file)),
                "userhash" : (None, '')
            }

            r = requests.post('https://catbox.moe/user/api.php', files=files)

            if r.text.find('http') != -1:
                link = r.text
                image = UserImage(user=user, link=link, tag=message)
                image.save()

                self.deleteSession()

                os.remove(imageName)

                self.setMessage([{
                    "message" : "Thanks! Your image has been saved!",
                    "type" : "text"
                }])

                self.send()
            else:
                raise Exception("Something happened. Your image can't be uploaded")